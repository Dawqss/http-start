import { Injectable } from "@angular/core";
import { Headers, Http, Response } from "@angular/http";
import 'rxjs/Rx'
import {Observable} from "rxjs/Observable";

@Injectable()
export class ServersService {
  constructor(private http: Http) {}
  

  storeServers() {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post('http://metalkas.dev/search-service.json', {});

  }

  getServers() {
    return this.http.get('http://metalkas.dev/search-service.json')
      .map(
        (response: Response) => {
          //tutaj wiadomo
        }
      ).catch(
        (error: Response) => {
          return Observable.throw(error)
        }
      )
  }
}